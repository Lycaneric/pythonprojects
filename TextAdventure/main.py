# Created 07/25/2016 - A text adventure from Eric Wideman

#System Imports
import time

#Program Start
print("Welcome to your epic adventure. Are you continuing on your quest or starting anew?")
time. sleep(1)
cont = input("Type 'C' to continue game or type 'N' for new game.")

#Determine New or Continue
if cont == "C" or cont == "c":
	print("There is nothing to continue at this time...")
elif cont == "N" or cont == "n":
	print("Let's start your adventure.")
else:
	print("Please select a valid answer.")
