# Dice Roller - Written By Eric Wideman 06/21/2016
#copy and paste the following into the command line:
#python C:\Users\ewideman\OneDrive\Programs\PythonProjects\DiceRoller.py

#imports
import time
import random

#welcome
print("Welcome to Dice Roller.")
time.sleep(2)
roll = True

#main loop starts here
while roll == True:
	#number of dice
	print("First, we will pick how many dice to roll...")
	time.sleep(2)
	dice_nbr = input("How many dice would you like to roll? ")
	time.sleep(1)
	print("You want to roll " + dice_nbr + " dice.")
	time.sleep(3)

	#number of sides
	print("Next, we will pick how many sides each die will have.")
	time.sleep(2)
	dice_sides = input("How many sides would you like? ")
	time.sleep(1)
	print("Each die will have " + dice_sides + " sides.")
	time.sleep(3)

	#variable
	total = 0

	#rolling
	for n in range(1, int(dice_nbr) + 1):
		n = random.randint(1, int(dice_sides) + 1)
		print("You rolled a(n) " + str(n) + ".")
		total += n
		time.sleep(1)
	
	#results
	time.sleep(3)
	print("Your results...")
	time.sleep(2)
	print("You rolled " + str(dice_nbr) + " dice and got a total of " + str(total) + "!")
	time.sleep(2)
	
	#options
	print("Would you like to roll again? ")
	time.sleep(1)
	roll_again = input("Enter 'Y' or 'N': ")
	time.sleep(1)
	
	#continue or break
	if roll_again == "Y" or roll_again == "y":
		roll = True
		continue
	else:
		roll = False
		break
	








k = input("Press enter to exit.")